#!/usr/bin/env bash

apt-get update -qq
apt-get -y -qq install git
curl -sS https://getcomposer.org/installer | php

yes | pecl install xdebug -f > /dev/null
echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini
echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini
echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

php composer.phar install
php -v

