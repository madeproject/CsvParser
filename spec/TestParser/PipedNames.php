<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 10:57
 */

namespace tests\TestParser;

use madeprojects\CSVParser\Converter\BaseConverter;
use madeprojects\CSVParser\Violation\Violation;

class PipedNames extends BaseConverter
{

	/**
	 * Does return the converted value or null if value is invalid
	 *
	 * @param string $value
	 * @return mixed
	 */
	public function convert ($value)
	{
		$this->reset();

		if(strpos($value, '|') === false){
			$this->violations[] = new Violation('No pipe found.');
			return null;
		}

		$names = explode('|', $value);
		$this->outputValue = implode(', ', $names);

		return $names;
	}
}