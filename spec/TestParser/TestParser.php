<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 26.06.16
 * Time: 23:19
 */

namespace tests\TestParser;

use madeprojects\CSVParser\CSVParser;

class TestParser extends CSVParser
{
	public function __construct ()
	{
		$this->columns = array();
		$this->requiredIf = array();
		$this->validators = collect();
		$this->converters = collect();
		$this->violations = collect();
	}

	public function setColumns (array $columns)
	{
		$this->columns = $columns;
	}

	public function setValidators ($validators)
	{
		$this->validators = collect($validators);
	}

	public function setConverters ($converters)
	{
		$this->converters = collect($converters);
	}

	public function setRowValidators ($validators)
	{
		$this->rowValidators = collect($validators);
	}

	public function setRequiredIf ($requiredIf)
	{
		$this->requiredIf = $requiredIf;
	}

}