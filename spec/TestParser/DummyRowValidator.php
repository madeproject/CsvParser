<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 28.06.16
 * Time: 10:02
 */

namespace tests\TestParser;


use Illuminate\Support\Collection;
use madeprojects\CSVParser\Rules\RowValidator;
use madeprojects\CSVParser\Violation\Violation;

class DummyRowValidator implements RowValidator
{

	/**
	 * Check the value and return a collection of violations if any
	 *
	 * @param Collection $value
	 * @return Collection
	 */
	public function check (Collection $value)
	{
		if($value['firstname'] != 'Benjamin' && $value['lastname'] != 'Milde') return collect([]);

		return collect([
			new Violation('Benjamin Milde is not allowed')
		]);
	}}