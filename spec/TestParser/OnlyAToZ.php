<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 26.06.16
 * Time: 23:22
 */

namespace tests\TestParser;

use Illuminate\Support\Collection;
use madeprojects\CSVParser\Rules\CellValidator;
use madeprojects\CSVParser\Violation\Violation;

class OnlyAToZ implements CellValidator
{

	/**
	 * Check the value and return a collection of violations if any
	 *
	 * @param $value
	 * @return Collection
	 */
	public function check ($value)
	{
		if(preg_match("/^[A-Za-z ]+$/", $value)) return collect([]);

		return collect([
			new Violation(sprintf('Only these characters allowed %s.', '[A-Za-z ]+'))
		]);
	}
}