<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 10:03
 */

namespace tests\TestParser;

use Illuminate\Support\Collection;
use madeprojects\CSVParser\Rules\CellValidator;
use madeprojects\CSVParser\Violation\Violation;

class Email implements CellValidator
{

	/**
	 * Check the value and return a collection of violations if any
	 *
	 * @param $value
	 * @return Collection
	 */
	public function check ($value)
	{
		if(filter_var($value, FILTER_VALIDATE_EMAIL)) return collect([]);

		return collect([
			new Violation(sprintf('Invalid email address "%s".', $value))
		]);
	}
}