<?php
namespace tests\TestParser;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use madeprojects\CSVParser\Cell;
use madeprojects\CSVParser\Violation\Violation;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

describe('TestParser', function(){

	describe('Exportabilit', function(){


		beforeEach(function () {
			$this->parser = new TestParser();

			$this->parser->setColumns(array(
				'firstCol', 'lastname', 'email'
			));

			$this->parser->setValidators(collect([]));
			$this->parser->setConverters(collect([
				'firstCol' => PipedNames::class
			]));

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Muller,mnet@example.com");
		});

		it('should be exportable as json', function(){
			expect($this->parser->toJson(true))
				->toEqual('[{"firstCol":"Benjamin","lastname":"Milde","email":"benni@kobrakai.de"},{"firstCol":"Hans91","lastname":"Muller","email":"mnet@example.com"}]');
			expect($this->parser->toJson())
				->toEqual('[{"firstCol":"Benjamin","lastname":"Milde","email":"benni@kobrakai.de"},{"firstCol":"Hans91","lastname":"Muller","email":"mnet@example.com"}]');
		});

		it('should be jsonable', function(){
			expect(json_encode($this->parser->toJsonable(true)))
				->toEqual('[{"firstCol":"Benjamin","lastname":"Milde","email":"benni@kobrakai.de"},{"firstCol":"Hans91","lastname":"Muller","email":"mnet@example.com"}]');
			expect(json_encode($this->parser->toJsonable()))
				->toEqual('[{"firstCol":"Benjamin","lastname":"Milde","email":"benni@kobrakai.de"},{"firstCol":"Hans91","lastname":"Muller","email":"mnet@example.com"}]');
			expect(json_encode($this->parser))
				->toEqual('[{"firstCol":"Benjamin","lastname":"Milde","email":"benni@kobrakai.de"},{"firstCol":"Hans91","lastname":"Muller","email":"mnet@example.com"}]');
		});

		it('should be exportable as csv', function(){
			expect((string) $this->parser->toCsv(true))
				->toEqual("Benjamin,Milde,benni@kobrakai.de\nHans91,Muller,mnet@example.com\n");
			expect((string) $this->parser->toCsv())
				->toEqual("Benjamin,Milde,benni@kobrakai.de\nHans91,Muller,mnet@example.com\n");
		});
	});

	describe('Validators', function(){

		beforeEach(function(){
			$this->parser = new TestParser();

			$this->parser->setColumns(array(
				'firstname', 'lastname', 'email'
			));

			$this->parser->setValidators(collect([
				array('column' => 'firstname', 'validator' => OnlyAToZ::class)
			]));
		});

		it('does detect issues in files', function(){
			$this->parser->parseFromFile(__DIR__ . '/assets/test.csv');
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation = $violations->first();

			expect($this->parser->hasViolation())->toBe(true);
			expect($violations->count())->toBe(1);
			expect($violation->getColumn())->toBe('firstname');
			expect($violation->getLine())->toBe(2);
			expect($violation->getMsg())->toBe("Only these characters allowed [A-Za-z ]+.");
		});

		it('does detect issues in strings', function(){
			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,mnet@example.com");
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation = $violations->first();

			expect($this->parser->hasViolation())->toBe(true);
			expect($violations->count())->toBe(1);
			expect($violation->getColumn())->toBe('firstname');
			expect($violation->getLine())->toBe(2);
			expect($violation->getMsg())->toBe("Only these characters allowed [A-Za-z ]+.");
		});

		it('does correctly detect valid data', function(){
			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans,Müller,mnet@example.com");
			$violations = $this->parser->validate();

			expect($this->parser->hasViolation())->toBe(false);
			expect($violations->count())->toBe(0);
		});

		it('does detect multiple issues', function(){
			$this->parser->setValidators(collect([
				['column' => 'firstname', 'validator' => OnlyAToZ::class],
				['column' => 'email', 'validator' => Email::class]
			]));

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,@example.com");
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation1 = $violations->get(0);
			$violation2 = $violations->get(1);

			expect($violations->count())->toBe(2);

			expect($violation1->getColumn())->toBe('firstname');
			expect($violation1->getLine())->toBe(2);
			expect($violation1->getMsg())->toBe("Only these characters allowed [A-Za-z ]+.");

			expect($violation2->getColumn())->toBe('email');
			expect($violation2->getLine())->toBe(2);
			expect($violation2->getMsg())->toBe("Invalid email address \"@example.com\".");
		});

		it('does detect multiple issues on single field', function(){
			$this->parser->setValidators(collect([
				['column' => 'firstname', 'validator' => OnlyAToZ::class],
				['column' => 'firstname', 'validator' => Email::class]
			]));

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,@example.com");
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation1 = $violations->get(0);
			$violation2 = $violations->get(1);
			$violation3 = $violations->get(2);

			expect($violations->count())->toBe(3);

			expect($violation1->getColumn())->toBe('firstname');
			expect($violation1->getLine())->toBe(1);
			expect($violation1->getMsg())->toBe("Invalid email address \"Benjamin\".");

			expect($violation2->getColumn())->toBe('firstname');
			expect($violation2->getLine())->toBe(2);
			expect($violation2->getMsg())->toBe("Only these characters allowed [A-Za-z ]+.");

			expect($violation3->getColumn())->toBe('firstname');
			expect($violation3->getLine())->toBe(2);
			expect($violation3->getMsg())->toBe("Invalid email address \"Hans91\".");
		});

		it('does detect rowissues', function(){
			$this->parser->setRowValidators(collect([DummyRowValidator::class]));
			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans,Müller,mnet@example.com");
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation = $violations->first();

			expect($this->parser->hasViolation())->toBe(true);
			expect($violations->count())->toBe(1);
			expect($violation->getColumn())->toBe(null);
			expect($violation->getLine())->toBe(1);
			expect($violation->getMsg())->toBe('Benjamin Milde is not allowed');
		});

		it('does detect rowissues with cell issues', function(){
			$this->parser->setRowValidators(collect([DummyRowValidator::class]));
			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,mnet@example.com");
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation1 = $violations->get(0);
			$violation2 = $violations->get(1);

			expect($this->parser->hasViolation())->toBe(true);
			expect($violations->count())->toBe(2);

			expect($violation1->getColumn())->toBe(null);
			expect($violation1->getLine())->toBe(1);
			expect($violation1->getMsg())->toBe('Benjamin Milde is not allowed');

			expect($violation2->getColumn())->toBe('firstname');
			expect($violation2->getLine())->toBe(2);
			expect($violation2->getMsg())->toBe("Only these characters allowed [A-Za-z ]+.");
		});

		it('does detect issues with already instanciated validators', function(){
			$this->parser->setValidators(collect([
				array('column' => 'firstname', 'validator' => new OnlyAToZ())
			]));
			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,mnet@example.com");
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation = $violations->first();

			expect($this->parser->hasViolation())->toBe(true);
			expect($violations->count())->toBe(1);
			expect($violation->getColumn())->toBe('firstname');
			expect($violation->getLine())->toBe(2);
			expect($violation->getMsg())->toBe("Only these characters allowed [A-Za-z ]+.");
		});

		it('does detect validation for required empty fields', function(){
			$this->parser->setValidators(collect([
				['column' => 'firstname', 'validator' => OnlyAToZ::class],
				['column' => 'email', 'validator' => Email::class]
			]));

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans,Müller,");
			$violations = $this->parser->validate();
			expect($violations->count())->toBe(2); // Email::class and RequiredValidator::class
		});

		it('does skip validation for non required empty fields', function(){
			$this->parser->setValidators(collect([
				['column' => 'firstname', 'validator' => OnlyAToZ::class],
				['column' => 'email', 'validator' => Email::class]
			]));

			$this->parser->setRequiredIf([
				'email' => false
			]);

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans,Müller,");
			$violations = $this->parser->validate();
			expect($violations->count())->toBe(0);
		});

		it('does not skip validation for non required incorrectly filled fields', function(){
			$this->parser->setValidators(collect([
				['column' => 'firstname', 'validator' => OnlyAToZ::class],
				['column' => 'email', 'validator' => Email::class]
			]));

			$this->parser->setRequiredIf([
				'email' => false
			]);

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans,Müller,@made.com");
			$violations = $this->parser->validate();
			expect($violations->count())->toBe(1);
		});

		it('requirement can be dynamically determined by the complete row', function(){
			$this->parser->setValidators(collect([
				['column' => 'firstname', 'validator' => OnlyAToZ::class],
				['column' => 'email', 'validator' => Email::class]
			]));

			$this->parser->setRequiredIf([
				'email' => function($key, $row){
					return $row['firstname'] == 'Benjamin'; // First is required, second not
				}
			]);

			$this->parser->parseFromString("Benjamin,Milde,\nHans,Müller,");
			$violations = $this->parser->validate();
			expect($violations->count())->toBe(2); // Email::class and RequiredValidator::class
		});

		it('does have a violation for empty required fields even without further validators', function(){
			$this->parser->setValidators(collect([]));
			$this->parser->setRequiredIf([]);

			$this->parser->parseFromString("Benjamin,Milde,benni\nHans,Müller,");
			$violations = $this->parser->validate();
			expect($violations->count())->toBe(1); // RequiredValidator::class
		});

	});

	describe('Converters', function() {

		beforeEach(function () {
			$this->parser = new TestParser();

			$this->parser->setColumns(array(
				'firstCol', 'lastname', 'email'
			));

			$this->parser->setValidators(collect([]));
			$this->parser->setConverters(collect([
				'firstCol' => PipedNames::class
			]));
		});

		it('should convert everything with NullConverter if no other one is supplied', function(){

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,@example.com");
			$this->parser->validate();

			$data = $this->parser->getData();
			$lookup = collect(['Milde', 'Müller']);

			$data->pluck('lastname')->each(function(Cell $node, $line) use($lookup) {
				$val = $lookup->get($line, '');
				expect((string) $node)->toBe($val);
				expect($node->getConvertedValue())->toBe($val);
				expect($node->getInitialValue())->toBe($val);
			});
		});

		it('should convert to a mixed values as well as a string for frontend output', function(){

			$this->parser->parseFromString("Hans|Manni,Milde,benni@kobrakai.de\nFrauke|Petra,Müller,@example.com");
			$this->parser->validate();

			$data = $this->parser->getData();
			$lookup = collect([
				['Hans|Manni', ['Hans', 'Manni'], 'Hans, Manni'],
				['Frauke|Petra', ['Frauke', 'Petra'], 'Frauke, Petra']
			]);

			$data->pluck('firstCol')->each(function(Cell $node, $line) use($lookup) {
				list($start, $converted, $output) = $lookup->get($line, array(null, null, null));
				expect($node->getInitialValue())->toBe($start);
				expect($node->getConvertedValue())->toBe($converted);
				expect((string) $node)->toBe($output);
			});
		});

		it('should also create violations if value is not valid', function(){

			$this->parser->parseFromString("Hans,Milde,benni@kobrakai.de\nFrauke|Petra,Müller,@example.com");
			$violations = $this->parser->validate();
			/** @var Violation $violation */
			$violation1 = $violations->get(0);

			expect($violations->count())->toBe(1);

			expect($violation1->getColumn())->toBe('firstCol');
			expect($violation1->getLine())->toBe(1);
			expect($violation1->getMsg())->toBe('No pipe found.');

			$data = $this->parser->getData();
			$lookup = collect([
				['Hans', null, 'Hans'],
				['Frauke|Petra', ['Frauke', 'Petra'], 'Frauke, Petra']
			]);

			$data->pluck('firstCol')->each(function(Cell $node, $line) use($lookup) {
				list($start, $converted, $output) = $lookup->get($line, array(null, null, null));
				expect($node->getInitialValue())->toBe($start);
				expect($node->getConvertedValue())->toBe($converted);
				expect((string) $node)->toBe($output);
			});
		});

		it('should reset on each conversion', function(){
			$converter = new PipedNames();

			$data = $converter->convert('Mike|Petra');

			expect($converter->getOutputValue())->toBe('Mike, Petra');
			expect($data)->toBe(['Mike', 'Petra']);

			$data = $converter->convert('Mike');

			expect($converter->getOutputValue())->toBe('');
			expect($data)->toBe(null);
		});

	});



	describe('Nodes', function() {

		beforeEach(function () {
			$this->parser = new TestParser();

			$this->parser->setColumns(array(
				'firstCol', 'lastname', 'email'
			));

			$this->parser->setValidators(collect([]));
			$this->parser->setConverters(collect([
				'firstCol' => PipedNames::class
			]));
		});

		it('should convert data fields to nodes', function(){

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,@example.com");

			$data = $this->parser->getData()->toArray();
			$nodes = new RecursiveIteratorIterator(new RecursiveArrayIterator($data));

			$lines = collect([1, 1, 1, 2, 2, 2]);

			foreach($nodes as $key => $node){
				expect($node)->toBeAnInstanceOf(Cell::class);
				expect($node->getLine())->toBe($lines->get($key, 0));
			}
		});

		it('should have correct line numbers and columns', function(){

			$this->parser->parseFromString("Benjamin,Milde,benni@kobrakai.de\nHans91,Müller,@example.com");

			$data = $this->parser->getData()->toArray();
			$nodes = new RecursiveIteratorIterator(new RecursiveArrayIterator($data));

			$lines = collect([1, 1, 1, 2, 2, 2]);
			$columns = collect(['firstCol', 'lastname', 'email', 'firstCol', 'lastname', 'email']);

			foreach($nodes as $key => $node){
				expect($node->getLine())->toBe($lines->get($key, 0));
				expect($node->getColumn())->toBe($columns->get($key, 0));
			}
		});

	});

});