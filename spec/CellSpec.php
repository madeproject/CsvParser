<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 14:32
 */

use madeprojects\CSVParser\Cell;

describe('Cell', function(){

	it('should correctly return column and line', function(){
		$node = Cell::createLocated('test', 1, 'col');

		expect($node->getLine())->toBe(1);
		expect($node->getColumn())->toBe('col');
	});

});