<?php

use madeprojects\CSVParser\Violation\Violation;

describe('Violation', function(){
	it('should be jsonable', function(){
		expect(json_encode(new Violation('Message')))->toBe('{"column":null,"line":null,"msg":"Message"}');
	});
});