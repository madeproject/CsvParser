<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 13:44
 */

use madeprojects\CSVParser\Violation\Violation;
use tests\TestParser\Email;

describe('EmailValidator', function(){

	beforeEach(function(){
		$this->validator = new Email();
	});

	it("should return an empty list of violations", function() {
		expect($this->validator->check('benni@example.com')->count())->toBe(0);
	});

	it("should return a single violations if invalid", function(){
		$violations = $this->validator->check('benni@example');
		expect($violations->count())->toBe(1);
		expect($violations->get(0))->toBeAnInstanceOf(Violation::class);
	});

});