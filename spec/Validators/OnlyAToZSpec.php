<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 00:08
 */

use madeprojects\CSVParser\Violation\Violation;
use tests\TestParser\OnlyAToZ;

describe('AToZValidator', function(){

	beforeEach(function(){
		$this->validator = new OnlyAToZ();
	});

	it("should return an empty list of violations", function() {
		expect($this->validator->check('Benjamin')->count())->toBe(0);
	});

	it("should return a single violations if invalid", function(){
		$violations = $this->validator->check('benni91');
		expect($violations->count())->toBe(1);
		expect($violations->get(0))->toBeAnInstanceOf(Violation::class);
	});

});