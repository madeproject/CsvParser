<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 13:44
 */

use madeprojects\CSVParser\Rules\RequiredValidator;
use madeprojects\CSVParser\Violation\Violation;

describe('RequiredValidator', function(){

	beforeEach(function(){
		$this->validator = new RequiredValidator();
	});

	it("should return an empty list of violations", function() {
		collect([
			'a',
			'%',
			'$',
			'0',
			'asdasdas',
			'13213eas'
		])->each(function($str){
			expect($this->validator->check($str)->count())->toBe(0);
		});
	});

	it("should return a single violations if invalid", function(){
		collect([
			''
		])->each(function($data){
			$violations = $this->validator->check($data);
			expect($violations->count())->toBe(1);
			expect($violations->get(0))->toBeAnInstanceOf(Violation::class);
		});

	});

});