<?php

use Kahlan\Plugin\Stub;
use madeprojects\CSVParser\Row;
use madeprojects\CSVParser\Violation\Violation;

describe('Row', function(){

	it('should hold it\'s row number', function(){
		foreach(range(0, 100) as $number){
			$row = Row::withLineNumber($number);
			expect($row->getLine())->toBe($number);
			$rand = rand(0, 100);
			expect($row->setLine($rand)->getLine())->toBe($rand);
		}
	});

	it('should hold violations on it\'s own', function(){
		$row = Row::withLineNumber(1);
		$violation = new Violation('Test');

		$row->addViolations(collect([$violation]));

		expect($row->getViolations()->count())->toBe(1);
		expect($row->hasViolation())->toBe(true);
		expect($row->getViolations()->search($violation))->not->toBe(false);
	});

});