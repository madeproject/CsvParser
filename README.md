# Usage [![build status](https://gitlab.com/madeproject/CsvParser/badges/master/build.svg)](https://gitlab.com/madeproject/CsvParser/commits/master)

Put this in your composer.json file.

```json
{
    "require": {
        "madeprojects/CsvParser": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:madeproject/CsvParser.git"
        }
    ]
}
```