<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 08:17
 */

namespace madeprojects\CSVParser\Converter;


use Illuminate\Support\Collection;

abstract class BaseConverter implements Converter
{
	/**
	 * @var Collection
	 */
	protected $violations = [];

	/**
	 * @var string
	 */
	protected $outputValue = '';

	/**
	 * Return the output value of the last conversion or an emptry string
	 *
	 * @return string
	 */
	public function getOutputValue ()
	{
		return $this->outputValue;
	}

	/**
	 * @return Collection
	 */
	public function getViolations ()
	{
		return collect($this->violations);
	}

	/**
	 * Reset the internal variables for a new conversion
	 */
	protected function reset ()
	{
		$this->violations = [];
		$this->outputValue = '';
	}
}