<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 08:15
 */

namespace madeprojects\CSVParser\Converter;


use Illuminate\Support\Collection;

interface Converter
{
	/**
	 * Does return the converted value or null if value is invalid
	 *
	 * @param string $value
	 * @return mixed
	 */
	public function convert($value);

	/**
	 * Return the output value of the last conversion or an emptry string
	 *
	 * @return string
	 */
	public function getOutputValue();

	/**
	 * Return a list of violations of the last conversion
	 *
	 * @return Collection
	 */
	public function getViolations();
}