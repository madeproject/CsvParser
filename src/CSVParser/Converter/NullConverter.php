<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 08:17
 */

namespace madeprojects\CSVParser\Converter;


class NullConverter extends BaseConverter
{
	/**
	 * @param string $value
	 * @return mixed
	 */
	public function convert ($value)
	{
		$this->outputValue = $value;
		return $value;
	}
}