<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 28.06.16
 * Time: 09:06
 */

namespace madeprojects\CSVParser;


use Illuminate\Support\Collection;
use madeprojects\CSVParser\Violation\Violatable;
use madeprojects\CSVParser\Violation\Violation;
use madeprojects\CSVParser\Violation\ViolationsTrait;

class Row extends Collection implements Violatable
{
	use ViolationsTrait;

	/**
	 * @var int
	 */
	private $line;

	/**
	 * Row constructor.
	 *
	 * @param array $items
	 */
	public function __construct ($items = [])
	{
		$this->violations = new Collection();
		$this->line = 0;
		parent::__construct($items);
	}

	/**
	 * @param int $line
	 */
	public static function withLineNumber ($line)
	{
		return (new static())->setLine($line);
	}

	/**
	 * @return Collection $violations
	 */
	public function getDownstreamViolations ()
	{
		return collect($this->all())->flatMap(function(Cell $cell){
			return $cell->getViolations();
		});
	}

	/**
	 * @param Violation $violation
	 * @return void
	 */
	public function defineViolationLocation (Violation $violation)
	{
		$violation->setLine($this->line);
	}

	/**
	 * @return int|null
	 */
	public function getLine ()
	{
		return $this->line;
	}

	/**
	 * @param mixed $line
	 * @return Violation
	 */
	public function setLine ($line)
	{
		$this->line = $line;

		return $this;
	}
}