<?php
namespace madeprojects\CSVParser;

use Illuminate\Support\Collection;
use JsonSerializable;
use League\Csv\Reader;
use League\Csv\Writer;
use madeprojects\CSVParser\Converter\Converter;
use madeprojects\CSVParser\Converter\NullConverter;
use madeprojects\CSVParser\Rules\CellValidator;
use madeprojects\CSVParser\Rules\RequiredValidator;
use madeprojects\CSVParser\Rules\RowValidator;
use madeprojects\CSVParser\Violation\Violatable;
use madeprojects\CSVParser\Violation\Violation;
use madeprojects\CSVParser\Violation\ViolationsTrait;

abstract class CSVParser implements Violatable, JsonSerializable
{
	use ViolationsTrait;

	const SEMICOLON = ";";
	const COMMA = ",";
	const TAB = "\t";

	/**
	 * @var Collection
	 */
	protected $data;

	/**
	 * @var array
	 */
	protected $columns;

	/**
	 * @var array
	 */
	protected $requiredIf;

	/**
	 * @var Collection
	 */
	protected $validators;

	/**
	 * @var Collection
	 */
	protected $rowValidators;

	/**
	 * @var Collection
	 */
	protected $converters;

	/**
	 * @param string $path
	 * @param string $delimiter
	 * @param string $enclosure
	 */
	public function parseFromFile ($path, $delimiter = ',', $enclosure = '"')
	{
		$reader = Reader::createFromPath($path);
		$reader->setDelimiter($delimiter);
		$reader->setEnclosure($enclosure);
		$rows = iterator_to_array($reader->fetchAssoc($this->columns));
		$this->parseData($rows);
	}

	/**
	 * @param string $str
	 * @param string $delimiter
	 * @param string $enclosure
	 */
	public function parseFromString ($str, $delimiter = ',', $enclosure = '"')
	{
		$reader = Reader::createFromString($str);
		$reader->setDelimiter($delimiter);
		$reader->setEnclosure($enclosure);
		$rows = iterator_to_array($reader->fetchAssoc($this->columns));
		$this->parseData($rows);
	}

	/**
	 * @param array $rows
	 */
	private function parseData ($rows)
	{
		$this->data = collect($rows)
			->map(function(array $row, $key){
				$lineNumber = $key + 1;
				$cellRow = Row::withLineNumber($lineNumber);
				foreach($row as $column => $value){
					$cellRow->put($column, Cell::createLocated($value, $lineNumber, $column));
				}
				return $cellRow;
			});
	}

	/**
	 * @return Collection
	 */
	public function validate ()
	{
		$this->validateData();
		$this->validateRows();
		$this->convertData();

		return $this->getViolations();
	}

	/**
	 * Run the validation on the whole file
	 */
	protected function validateData ()
	{
		$this->withData('validateRow');
	}

	/**
	 * Run the validation on the whole file
	 */
	protected function convertData ()
	{
		$this->withData('convertRow');
	}

	/**
	 * Iterate over all data nodes and run a class method with it.
	 * @param $method
	 */
	private function withData ($method)
	{
		$this->data->each(function($row) use($method) {
			call_user_func([$this, $method], $row);
		});
	}

	/**
	 * @return Collection
	 */
	public function getData ()
	{
		return $this->data;
	}

	/**
	 * Validate a single row as a whole
	 */
	protected function validateRows ()
	{
		if(!$this->rowValidators || !$this->rowValidators->count()) return;

		$this->data->each(function(Row $row){
			$violations = $this->rowValidators
				->unique()
				->transform([$this, 'instantiateIfNeeded'])
				->flatMap(function(RowValidator $validator) use($row) {
					return $validator->check($row);
				});

			$row->addViolations($violations);
		});
	}

	/**
	 * Validate a single row's cells
	 *
	 * @param $row
	 */
	protected function validateRow ($row)
	{
		collect($row)
			->filter(function(Cell $cell, $key) use ($row) {
				if(strlen($cell->getInitialValue())) return true; // Always test available values
				if(!isset($this->requiredIf[$key])) return true; // Required by default
				$req = $this->requiredIf[$key];

				return is_callable($req)
					? $req($key, $row)
					: !!$req;
			})
			->each(function(Cell $cell, $key){
				$violations = $this->getValidatorsFor($key)
					->flatMap(function(CellValidator $validator) use($cell) {
						return $validator->check($cell->getInitialValue());
					});
				$cell->addViolations($violations);
			});
	}

	/**
	 * Validate a single row
	 *
	 * @param $row
	 */
	protected function convertRow ($row)
	{
		if(!$this->converters || !$this->converters->count()) return;

		collect($row)->each(function(Cell $cell, $key){
			$cell->convertWith($this->getConverterFor($key));
		});
	}

	/**
	 * Get all validators for that column
	 *
	 * @param $key
	 * @return Collection
	 */
	private function getValidatorsFor ($key)
	{
		return $this->validators
			->where('column', $key)
			->pluck('validator')
			->unique()
			->prepend(RequiredValidator::class)
			->transform([$this, 'instantiateIfNeeded']);
	}

	/**
	 * Get all validators for that column
	 *
	 * @param $key
	 * @return Converter
	 */
	private function getConverterFor ($key)
	{
		$className = $this->converters
			->get($key, NullConverter::class);
		return $this->instantiateIfNeeded($className);
	}

	/**
	 * @return Collection $violations
	 */
	public function getDownstreamViolations ()
	{
		return $this->data
			->flatMap(function(Row $row){
				return $row->getViolations();
			});
	}

	/**
	 * @param string|object $class
	 * @return object
	 */
	public function instantiateIfNeeded ($class)
	{
		if(is_string($class)) return new $class;
		return $class;
	}

	/**
	 * @param Violation $violation
	 * @return void
	 */
	public function defineViolationLocation (Violation $violation){}

	/**
	 * Convert data to csv
	 *
	 * @param bool $initial
	 * @return static
	 */
	public function toCsv ($initial = false)
	{
		$writer = Writer::createFromString('');
		foreach ($this->toJsonable($initial) as $row) {
			$writer->insertOne($row->toArray());
		}

		return $writer;
	}

	/**
	 * Convert data to jsonable array
	 *
	 * @param bool $initial
	 * @return string
	 */
	public function toJsonable ($initial = false)
	{
		$data = $this->getData();
		return $data->map(function($row) use($initial) {
			return $initial
				? $row->map(function(Cell $cell){ return $cell->getInitialValue(); })
				: $row->map(function(Cell $cell){ return (string) $cell; });
		});
	}

	/**
	 * @param bool $initial
	 * @return string
	 */
	public function toJson ($initial = false)
	{
		return json_encode($this->toJsonable($initial));
	}

	/**
	 * Specify data which should be serialized to JSON
	 *
	 * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 *        which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize ()
	{
		return $this->toJsonable();
	}
}