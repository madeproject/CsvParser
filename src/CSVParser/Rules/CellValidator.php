<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 26.06.16
 * Time: 23:13
 */

namespace madeprojects\CSVParser\Rules;


use Illuminate\Support\Collection;

interface CellValidator
{
	/**
	 * Check the value and return a collection of violations if any
	 * @param string $value
	 * @return Collection
	 */
	public function check ($value);
}