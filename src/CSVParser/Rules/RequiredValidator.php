<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 02.07.16
 * Time: 16:39
 */

namespace madeprojects\CSVParser\Rules;


use Illuminate\Support\Collection;
use madeprojects\CSVParser\Violation\Violation;

class RequiredValidator implements CellValidator
{

	/**
	 * Check the value and return a collection of violations if any
	 *
	 * @param string $value
	 * @return Collection
	 */
	public function check ($value)
	{
		if(strlen($value) > 0) return collect([]);

		return collect([
			new Violation('This field is mandatory.')
		]);
	}
}