<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 26.06.16
 * Time: 23:13
 */

namespace madeprojects\CSVParser\Rules;


use Illuminate\Support\Collection;

interface RowValidator
{
	/**
	 * Check the value and return a collection of violations if any
	 * @param Collection $value
	 * @return Collection
	 */
	public function check (Collection $value);
}