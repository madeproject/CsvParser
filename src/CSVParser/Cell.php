<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 27.06.16
 * Time: 08:02
 */

namespace madeprojects\CSVParser;


use Illuminate\Support\Collection;
use madeprojects\CSVParser\Converter\Converter;
use madeprojects\CSVParser\Violation\Violatable;
use madeprojects\CSVParser\Violation\Violation;
use madeprojects\CSVParser\Violation\ViolationsTrait;

class Cell implements Violatable
{
	use ViolationsTrait;

	/**
	 * @var int
	 */
	private $line;

	/**
	 * @var string
	 */
	private $column;

	/**
	 * @var string
	 */
	private $initialValue;

	/**
	 * @var mixed
	 */
	private $convertedValue;

	/**
	 * @var string
	 */
	private $outputValue;

	/**
	 * Node constructor.
	 *
	 * @param string $initialValue
	 */
	public function __construct ($initialValue)
	{
		$this->initialValue = (string) $initialValue;
		$this->violations = new Collection();
	}

	/**
	 * Named constructor
	 *
	 * @param $value
	 * @param $line
	 * @param $column
	 * @return Cell
	 */
	public static function createLocated ($value, $line, $column)
	{
		return (new static($value))->setLine($line)->setColumn($column);
	}

	/**
	 * @return Collection $violations
	 */
	public function getDownstreamViolations ()
	{
		return collect([]);
	}

	/**
	 * @param Violation $violation
	 * @return void
	 */
	public function defineViolationLocation (Violation $violation)
	{
		$violation->setColumn($this->column)->setLine($this->line);
	}

	/**
	 * @return int|null
	 */
	public function getLine ()
	{
		return $this->line;
	}

	/**
	 * @return string|null
	 */
	public function getColumn ()
	{
		return $this->column;
	}

	/**
	 * @return string
	 */
	public function getInitialValue ()
	{
		return $this->initialValue;
	}

	/**
	 * @return mixed
	 */
	public function getConvertedValue ()
	{
		return $this->convertedValue;
	}

	/**
	 * @param string $initialValue
	 */
	public function setInitialValue ($initialValue)
	{
		$this->initialValue = $initialValue;
	}

	/**
	 * @param mixed $line
	 * @return Violation
	 */
	public function setLine ($line)
	{
		$this->line = $line;

		return $this;
	}

	/**
	 * @param mixed $column
	 * @return Violation
	 */
	public function setColumn ($column)
	{
		$this->column = $column;

		return $this;
	}

	/**
	 * @param Converter $converter
	 */
	public function convertWith (Converter $converter)
	{
		$this->convertedValue = $converter->convert($this->initialValue);
		$this->outputValue = $converter->getOutputValue();

		if($this->convertedValue === null){
			$this->addViolations($converter->getViolations());
		}
	}

	/**
	 * @return string
	 */
	public function __toString ()
	{
		return $this->outputValue ?: $this->initialValue;
	}
}