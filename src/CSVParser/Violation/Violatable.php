<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 28.06.16
 * Time: 10:16
 */

namespace madeprojects\CSVParser\Violation;


use Illuminate\Support\Collection;

interface Violatable
{
	/**
	 * @param Collection $violations
	 * @return void
	 */
	public function addViolations (Collection $violations);

	/**
	 * @param bool $own If true only return own violations without downstream ones
	 * @return Collection $violations
	 */
	public function getViolations ($own = false);

	/**
	 * @return Collection $violations
	 */
	public function getDownstreamViolations ();

	/**
	 * @return bool
	 */
	public function hasViolation ();

	/**
	 * @param Violation $violation
	 * @return void
	 */
	public function defineViolationLocation (Violation $violation);
}