<?php
/**
 * Created by PhpStorm.
 * User: benni
 * Date: 28.06.16
 * Time: 10:18
 */

namespace madeprojects\CSVParser\Violation;


use Illuminate\Support\Collection;

trait ViolationsTrait
{
	/**
	 * @var Collection
	 */
	protected $violations;

	/**
	 * This does depend on the Violatable Interface
	 * @param Collection $violations
	 */
	public function addViolations (Collection $violations)
	{
		$violations = $violations
			->filter(function($violation){
				return $violation instanceof Violation;
			})
			->each([$this, 'defineViolationLocation']);

		$this->violations = $this->violations->merge($violations);
	}

	/**
	 * @param bool $own
	 * @return Collection
	 */
	public function getViolations ($own = false)
	{
		if($own) return $this->violations;
		return $this->violations->merge($this->getDownstreamViolations());
	}

	/**
	 * @return bool
	 */
	public function hasViolation ()
	{
		return !$this->getViolations()->isEmpty();
	}
}