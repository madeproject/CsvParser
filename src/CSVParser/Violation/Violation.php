<?php
namespace madeprojects\CSVParser\Violation;

use JsonSerializable;

class Violation implements JsonSerializable
{
	private $msg;
	private $line;
	private $column;

	/**
	 * Violation constructor.
	 *
	 * @param $msg
	 */
	public function __construct ($msg)
	{
		$this->msg = $msg;
	}

	/**
	 * @return mixed
	 */
	public function getLine ()
	{
		return $this->line;
	}

	/**
	 * @param mixed $line
	 * @return Violation
	 */
	public function setLine ($line)
	{
		$this->line = $line;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getColumn ()
	{
		return $this->column;
	}

	/**
	 * @param mixed $column
	 * @return Violation
	 */
	public function setColumn ($column)
	{
		$this->column = $column;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getMsg ()
	{
		return $this->msg;
	}

	/**
	 * Specify data which should be serialized to JSON
	 *
	 * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 *        which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	function jsonSerialize ()
	{
		return ['column' => $this->getColumn(), 'line' => $this->getLine(), 'msg' => $this->getMsg()];
	}
}